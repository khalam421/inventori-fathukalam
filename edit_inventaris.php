<?php
include "koneksi.php";
$id_inventaris =$_GET['id_inventaris'];

$data1= mysqli_query($conn,"select * from invantaris where id_inventaris='$id_inventaris'");
$data= mysqli_fetch_array($data1);
?>
<!DOCTYPE html>
<html>
    
<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/form-validation.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:17:09 GMT -->
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Aplikasi Ivantarisir</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="assets/css/style.css" rel="stylesheet" type="text/css">

    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="index.php" class="logo"><img src="assets/images/logo.png" height="24" alt="logo"></a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="assets/images/users/avatar-6.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16">Fathukalam</h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <div id="sidebar-menu">
                        <ul>
                            <li class="menu-title">Menu</li>

                            <li>
                                <a href="index.php" class="waves-effect">
                                    <i class="ti-home"></i>
                                    <span> Dashboard <span class="badge badge-primary pull-right">3</span></span>
                                </a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i> <span> Pegawai</span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="data_pegawai.php">Data Pegawai</a></li>
                                    <li><a href="ui-grid.html">Grid</a></li>
                                </ul>
                            </li>
                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span>Petugas</span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="data_petugas.php">Data Petugas</a></li>
                                    <li><a href="ui-grid.html">Grid</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-crown"></i> <span> Transaksi </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="advanced-animation.html">Peminjaman</a></li>
                                    <li><a href="advanced-highlight.html">Pengembalian</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-layout"></i><span> Ivantarisir </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                         
                                     <li><a href="tampil_data.php">Data Barang </a></li>
                                      <li><a href="data_jenis.php">Data Jenis</a></li>
                                      <li><a href="ruang.php">Data Ruang</a></li>
                                 
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span> Login </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="login.php">Login</a></li>
                                    <li><a href="register.php">Register</a></li>
                                    <li><a href="#">Recover Password</a></li>
                                    <li><a href="#">Laporan</a></li>
                                </ul>
                            </li>

                        </ul>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <i class="mdi mdi-email-outline noti-icon"></i>
                                        <span class="badge badge-danger noti-icon-badge">5</span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                        <!-- item-->
                                        <div class="dropdown-item noti-title">
                                            <h5><span class="badge badge-danger float-right">745</span>Messages</h5>
                                        </div>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon"><img src="assets/images/users/avatar-2.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                            <p class="notify-details"><b>Charles M. Jones</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon"><img src="assets/images/users/avatar-3.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                            <p class="notify-details"><b>Thomas J. Mimms</b><small class="text-muted">You have 87 unread messages</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon"><img src="assets/images/users/avatar-4.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                            <p class="notify-details"><b>Luis M. Konrad</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                        </a>

                                        <!-- All-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            View All
                                        </a>

                                    </div>
                                </li>

                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <i class="mdi mdi-bell-outline noti-icon"></i>
                                        <span class="badge badge-success noti-icon-badge">3</span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                        <!-- item-->
                                        <div class="dropdown-item noti-title">
                                            <h5><span class="badge badge-danger float-right">87</span>Notification</h5>
                                        </div>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                            <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-success"><i class="mdi mdi-message"></i></div>
                                            <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                                        </a>

                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            <div class="notify-icon bg-warning"><i class="mdi mdi-martini"></i></div>
                                            <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                        </a>

                                        <!-- All-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            View All
                                        </a>

                                    </div>
                                </li>

                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a>
                                        <a class="dropdown-item" href="#"><span class="badge badge-success pull-right">5</span><i class="mdi mdi-settings m-r-5 text-muted"></i> Settings</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5 text-muted"></i> Lock screen</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                    </div>
                                </li>

                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Form Validation</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card m-b-30">
                                        <div class="card-body">

                                            <form class="" action="update_inventaris.php?id_inventaris=<?php echo $id_inventaris;?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input id="nama" type="text" class="form-control" name="nama" required placeholder="Type something"
													value="<?php echo $data['nama'];?>"/>
                                                </div>
												 <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Kondisi</label>
                                                <div class="col-sm-10">
                                                    <select name ="kondisi" class="form-control" value="<?php echo $data['nama'];?>" >
                                                        <option>-</option>
                                                        <option>Baik</option>
                                                        <option>Rusak</option>
                                                    </select>
                                                </div>
                                            </div>
                                                <div class="form-group">
                                                    <label>Spesifikasi</label>
                                                    <input id="spesifikasi" type="text" class="form-control"  name="spesifikasi" required placeholder="Type something"
                                                    value="<?php echo $data['spesifikasi'];?>"/>

                                                </div>
												
												<div class="form-group">
                                                    <label>Keterangan</label>
                                                    <input id="keterangan" type="text" class="form-control"  name="keterangan" required placeholder="Type something"
													value="<?php echo $data['keterangan'];?>"/>
                                                </div>
                                               <div class="form-group">
                                                    <label>Jumlah</label>
                                                    <input id="jumlah" type="text" class="form-control"  name="jumlah" required placeholder="Type something"
													value="<?php echo $data['jumlah'];?>"/>
                                                </div>
												
												
												
												 <?php
                                                  include "koneksi.php";
                                                  $result = mysqli_query($conn,"select * from jenis order by id_jenis asc ");
                                                  $jsArray = "var id_jenis = new Array();\n";
                                                  ?>
												<div class="form-group">
                                                    
                                                    <select class="form-control"  name="id_jenis"onchange="changeValue(this.value)"/>
													<option selected="selected"><?php echo $data['id_jenis'];?>
                                                  <?php 
                                                  while($row = mysqli_fetch_array($result)){
                                                    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                    $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                  }
                                                  ?>
                                                </option>
                                                </div>
												<div class="form-group">
                                                    <label>Tanggal</label>
                                                    <input id="tgl_register" type="date" class="form-control"  name="tgl_register" required placeholder="Tanggal"
													value="<?php echo $data['tgl_register'];?>"/>
												</div>
													
													  <?php
                                                      include "koneksi.php";
                                                      $result = mysqli_query($conn,"select * from ruang order by id_ruang asc ");
                                                      $jsArray = "var id_ruang = new Array();\n";
                                                      ?>
													
													<div class="form-group">
                                                   
                                                     <select class="form-control"  name="id_ruang" onchange="changeValue(this.value)"/>
													<option selected="selected"><?php echo $data['id_ruang'];?>
                                                  <?php 
                                                      while($row = mysqli_fetch_array($result)){
                                                        echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                        $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                      }
                                                      ?>
                                                </option>
													</div>
													
													
												<div class="form-group">
                                                    <label>Kode Ivantaris</label>
                                                    <input id="kode_inventaris" type="text" class="form-control"  name="kode_inventaris" required placeholder="Kode Ivantaris"
													value="<?php echo $data['kode_inventaris'];?>"/>
                                                </div>
												
												<?php
                                                      include "koneksi.php";
                                                      $result = mysqli_query($conn,"select * from petugas order by id_petugas asc ");
                                                      $jsArray = "var id_petugas = new Array();\n";
                                                      ?>
												
												
												<div class="form-group">
                                                    <label>Id Petugas</label>
                                                      <select class="form-control"  name="id_petugas"onchange="changeValue(this.value)"/>
													<option selected="selected"><?php echo $data['id_petugas'];?>
                                                 <?php 
                                                      while($row = mysqli_fetch_array($result)){
                                                        echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                        $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                      }
                                                      ?>

                                                </option>
                                                </div>
                                                    <div class="form-group">
                                                    <label>Sumber</label>
                                                    <input id="sumber" type="text" class="form-control"  name="sumber" required placeholder="Sumber"
                                                    value="<?php echo $data['sumber'];?>"/>
                                                </div>
                   
                                                <div class="form-group">
                                                    <div>
                                                           <div class="form-actions text-right pal">
                                                <input name="simpan" type="submit" id="simpan" class="btn btn-primary"></input>                                                      
                                            </div>
                                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div> <!-- end col -->

                            </div> <!-- end row -->

                        </div><!-- container -->


                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <footer class="footer">
                    © 2018 Upcube - Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesdesign.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/modernizr.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- Parsley js -->
        <script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('form').parsley();
            });
        </script>

        <!-- App js -->
        <script src="assets/js/app.js"></script>

    </body>

<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/form-validation.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:17:09 GMT -->
</html>