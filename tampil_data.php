<!DOCTYPE html>
<html>
    
<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:17:32 GMT -->
<head> 
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Aplikasi Ivantaris</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- DataTables -->
        <link href="assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="assets/css/style.css" rel="stylesheet" type="text/css">

    </head>

    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="index.php" class="logo"><img src="assets/images/logo.png" height="24" alt="logo"></a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="assets/images/users/avatar-6.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16">Fathukalam</h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <div id="sidebar-menu">
                        <ul>
                            <li class="menu-title">Menu</li>

                            <li>
                                <a href="index.php" class="waves-effect">
                                    <i class="ti-home"></i>
                                    <span> Dashboard <span class="badge badge-primary pull-right">3</span></span>
                                </a>
                            </li>

                              <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i> <span> Pegawai</span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="data_pegawai.php">Data Pegawai</a></li>
                                  
                                </ul>
                            </li>
                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span>Petugas</span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="data_petugas.php">Data Petugas</a></li>
                               
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-crown"></i> <span> Transaksi </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="tampil_data_pinjam.php">Peminjaman</a></li>
                                    <li><a href="tampil_detail_data.php">Pengembalian</a></li>
                                </ul>
                            </li><li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-crown"></i> <span> Transaksi </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="tampil_data_pinjam.php">Peminjaman</a></li>
                                    <li><a href="tampil_detail_data.php">Pengembalian</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-layout"></i><span> Ivantarisir </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                         
                                     <li><a href="tampil_data.php">Data Barang </a></li>
                                      <li><a href="data_jenis.php">Data Jenis</a></li>
                                      <li><a href="ruang.php">Data Ruang</a></li>
                                 
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span> Laporan </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="#">Laporan</a></li>
                                </ul>
                            </li>


                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">


                                

                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <a class="dropdown-item" href="logout.php"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                    </div>
                                </li>

                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Datatable</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper "> 
                        <div class="container-fluid"> 

                            <div class="row">
                                <div class="col-12">
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                        <div class="table-responsive"> 
                                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                     <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Kondisi</th>
                                                    <th>Spesifikasi</th>
                                                    <th>Keterangan</th>
                                                    <th>Jumlah</th>
                                                    <th>Id Jenis</th>
                                                    <th>Tanggal</th>
                                                    <th>Id Ruang</th>
                                                    <th>Kode Inventaris</th>
                                                    <th>Id Petugas</th> 
                                                    <th>Sumber</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>

<?php
    include "koneksi.php";
    $no=1;
    $data1 = mysqli_query($conn, "select * from invantaris order by id_inventaris desc");
    while($data=mysqli_fetch_array($data1))
    {
    ?>

                                                 <tr>
                                            <td><?php echo $no++;?></td>
                                            <td><?php echo $data['nama'];?></td>
                                            <td><?php echo $data['kondisi'];?></td>
                                            <td><?php echo $data['spesifikasi'];?></td>
                                            <td><?php echo $data['keterangan'];?></td>
                                            <td><?php echo $data['jumlah'];?></td>
                                            <td><?php echo $data['id_jenis'];?></td>
                                            <td><?php echo $data['tgl_register'];?></td>
                                            <td><?php echo $data['id_ruang'];?></td>
                                            <td><?php echo $data['kode_inventaris'];?></td>
                                            <td><?php echo $data['id_petugas'];?></td>
                                            <td><?php echo $data['sumber'];?></td>
                                            <td><a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris'];?>">Edit</a>
                                            
                                            </td>
                                        </tr> 
                                        <?php
    }
    ?>                    
                                                </tbody>
                                            </table>
                                            </div>
                                            <div class="col-sm-0 col-md-0 m-t-10">
                                                    <div class="text-center">
                                                         <!-- Large modal -->
                                                        <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg">Tambah</button>
                                                    </div>

                                                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Tambah</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                </div>
                                                                <div class="modal-body">
                                                                                       <div class="page-content-wrapper ">

                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card m-b-0">
                                        <div class="card-body">

                                            <form class="" action="proses.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input id="nama" type="text" class="form-control" name="nama" required placeholder="Type something"/>
                                                </div>
                                                 <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Kondisi</label>
                                                <div class="col-sm-10">
                                                    <select name ="kondisi" class="form-control"  >
                                                        <option>-</option>
                                                        <option>Baik</option>
                                                        <option>Rusak</option>
                                                    </select>
                                                </div>
                                            </div>
                                                <div class="form-group">
                                                    <label>Spesifikasi</label>
                                                    <input id="spesifikasi" type="text" class="form-control"  name="spesifikasi" required placeholder="Type something"/>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Keterangan</label>
                                                    <input id="keterangan" type="text" class="form-control"  name="keterangan" required placeholder="Type something"/>
                                                </div>
                                               <div class="form-group">
                                                    <label>Jumlah</label>
                                                    <input id="jumlah" type="text" class="form-control"  name="jumlah" required placeholder="Type something"/>
                                                </div>
                                                 <?php
                                                  include "koneksi.php";
                                                  $result = mysqli_query($conn,"select * from jenis order by id_jenis asc ");
                                                  $jsArray = "var id_jenis = new Array();\n";
                                                  ?>
                                                <div class="form-group">
                                                    
                                                    <select class="form-control"  name="id_jenis"onchange="changeValue(this.value)"/>
                                                    <option selected="selected">..........Pilih Jenis..........
                                                  <?php 
                                                  while($row = mysqli_fetch_array($result)){
                                                    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                    $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                  }
                                                  ?>
                                                </option>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tanggal</label>
                                                    <input id="tgl_register" type="date" class="form-control"  name="tgl_register" required placeholder="Tanggal"/>
                                                </div>
                                                    
                                                      <?php
                                                      include "koneksi.php";
                                                      $result = mysqli_query($conn,"select * from ruang order by id_ruang asc ");
                                                      $jsArray = "var id_ruang = new Array();\n";
                                                      ?>
                                                    
                                                    <div class="form-group">
                                                   
                                                     <select class="form-control"  name="id_ruang" onchange="changeValue(this.value)"/>
                                                    <option selected="selected">..........Pilih Ruang..........
                                                  <?php 
                                                      while($row = mysqli_fetch_array($result)){
                                                        echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                        $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                      }
                                                      ?>
                                                </option>
                                                
                                            </select>
                                                    </div>
                                                    
													 <?php
                                                        $conn = mysqli_connect('localhost','root','','ujikom');

                                                        $cari_kd = mysqli_query($conn,"select max(kode_inventaris) as kode from invantaris");

                                                        $tm_cari= mysqli_fetch_array($cari_kd);
                                                        $kode=substr($tm_cari['kode'], 3,4);

                                                        $tambah=$kode+1;

                                                        if ($tambah<10) {
                                                            $kode_inventaris="BRG000".$tambah;
                                                        } else {
                                                            $kode_inventaris="BRG00".$tambah;
                                                        }
                                                        ?>
                                                    
                                                <div class="form-group">
                                                    <label>Kode Ivantaris</label>
                                                    <input id="kode_inventaris" type="text" class="form-control"  name="kode_inventaris" required placeholder="Kode Ivantaris" value="<?php echo $kode_inventaris ?>" />
                                                </div>
                                                
                                                <?php
                                                      include "koneksi.php";
                                                      $result = mysqli_query($conn,"select * from petugas order by id_petugas asc ");
                                                      $jsArray = "var id_petugas = new Array();\n";
                                                      ?>
                                                
                                                
                                                <div class="form-group">
                                                    <label>Id Petugas</label>
                                                      <select class="form-control"  name="id_petugas"onchange="changeValue(this.value)"/>
                                                    <option selected="selected">..........Pilih Id Petugas..........
                                                 <?php 
                                                      while($row = mysqli_fetch_array($result)){
                                                        echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                        $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                      }
                                                      ?>

                                                </option>
                                                </div>

                                               <div class="form-group">
                                                    <label>Sumber</label>
                                                    <input id="sumber" type="text" class="form-control"  name="sumber" required placeholder="Sumber"/>
                                                </div>
                                                <div class="form-group">
                                                    <div>
                                                           <div class="form-actions text-right pal">
                                                <input name="simpan" type="submit" id="simpan" class="btn btn-primary"></input>                                                      
                                            </div>
                                                            
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div> <!-- end col -->

                            </div> <!-- end row -->

                        </div><!-- container -->
</div></div></div></div>

                                        </div>
                                    </div>
                                </div> <!-- end col -->
                            </div> <!-- end row -->

                        </div><!-- container -->


                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <footer class="footer">
                    © 2019 Aplikasi Ivantaris <i class="mdi mdi-heart text-danger"></i> by Fathukalam
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/modernizr.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- Required datatable js -->
        <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="assets/plugins/datatables/jszip.min.js"></script>
        <script src="assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="assets/plugins/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Datatable init js -->
        <script src="assets/pages/datatables.init.js"></script>


        <!-- App js -->
        <script src="assets/js/app.js"></script>

    </body>

<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:17:40 GMT -->
</html>