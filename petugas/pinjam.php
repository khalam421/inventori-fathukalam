<!DOCTYPE html>
<html>
    
<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:15:41 GMT -->
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Aplikasi Ivantaris</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="../shortcut icon" href="../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="../stylesheet" href="../assets/plugins/morris/morris.css">

        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css">

    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="index.php" class="logo"><img src="../assets/images/logo.png" height="24" alt="logo"></a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../assets/images/users/avatar-6.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16">Fathukalam</h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <div id="sidebar-menu">
                        <ul>
                            

                           
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-crown"></i> <span> Peminjaman </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="data_pinjam.php">Tambah Peminjaman</a></li>
                                    <li><a href="pinjam.php">Peminjaman</a></li>
                                    
                                </ul>
                            </li>

                          
                           
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
  
                                </li>

                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <img src="../assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <a class="dropdown-item" href="logout.php"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                    </div>
                                </li>

                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Dashboard</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">
                          <div class="container-fluid">

                            <div class="row">
                   <div class="col-xl-4">
                                    <div class="card m-b-30">
                                        <div class="card-body">
                                  <form class="" action="prostamb_detpin.php" method="post" enctype="multipart/form-data" name="form1" id="form1">    
                                            
                                                 <div class="form-group">
                                                    <label>Nama Inventaris</label>
                                                    <select id="id_inventaris"  class="form-control" name="id_inventaris" required placeholder="Type something"/>
                                                    <option>-</option>
                                                    <?php
                                                    include "koneksi.php";
                                                    $select=mysqli_query($conn,"SELECT * FROM invantaris");
                                                    while ($show=mysqli_fetch_array($select)) {
                                                        ?>
                                                        <option value="<?=$show['id_inventaris'];?>"><?$show['id_inventaris'];?>. <?=$show['nama'];?></option>
                                                    <?php } ?>
                                                </div>
                                                <br></br>
                                                <br></br>
                                                <div class="form-group">
                                                    <label>Jumlah</label>
                                                    <input id="jumlah" type="number" class="form-control" name="jumlah" required placeholder="Type Jumlah"/>
                                                    
                                                </div>
                                                  <div class="form-group">
                                                    <label>ID Peminjaman</label>
                <?php
                $id_peminjaman = $_GET['id_peminjaman'];
                include "koneksi.php";
                $select=mysqli_query($conn, "SELECT * FROM  peminjaman where id_peminjaman='$id_peminjaman'");
                while($show=mysqli_fetch_array($select)){
                ?>
                                                    <input id="id_peminjaman" type="text" class="form-control" name="id_peminjaman" value="<?php echo $show['id_peminjaman']?>" required placeholder="Type something"/> 

                                                </div> <?php } ?>
                                                  
                                                    
                                                           <div class="form-actions text-right pal">
                                                <input name="simpan" type="submit" id="simpan" class="btn btn-primary"></input>                                                      
                                            </div>

                                  </form>

                                           

                                            <div id="morris-donut-example" style="height: 265px"></div>

                                        </div>

                                    </div>

                                </div>

                                 
                            </div>
                            <!-- end row -->
 <div class="row">
                                <div class="col-md-5">
                                    <div class="card m-b-25">
                                        <div class="card-body">
                                        <div class="table-responsive"> 
                                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                     <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Jumlah</th>
                                                    <th>Tanggal</th>
                                                    <th>Kode Inventaris</th>            
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>

<?php
    include "koneksi.php";
    $no=1;
    $data1 = mysqli_query($conn, "select * from invantaris order by id_inventaris desc");
    while($data=mysqli_fetch_array($data1))
    {
    ?>

                                                 <tr>
                                            <td><?php echo $no++;?></td>
                                            <td><?php echo $data['nama'];?></td>
                                            <td><?php echo $data['jumlah'];?></td>
                                            <td><?php echo $data['tgl_register'];?></td>
                                            <td><?php echo $data['kode_inventaris'];?></td>
                                            <td><a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris'];?>">Edit</a>
                                            
                                            </td>
                                        </tr> 
                                        <?php
    }
    ?>                    
                                                </tbody>
                                            </table>
                                            </div>

                        </div><!-- container -->


                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <footer class="footer">
                    © 2019 Aplikasi Ivantaris <i class="mdi mdi-heart text-danger"></i> by Fathukalam
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/popper.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!--Morris Chart-->
        <script src="../assets/plugins/morris/morris.min.js"></script>
        <script src="../assets/plugins/raphael/raphael-min.js"></script>

        <script src="../assets/pages/dashborad.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.js"></script>

    </body>

<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:16:28 GMT -->
</html>