<?php
include"koneksi.php";

?>
<!DOCTYPE html>
<html>
    
<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:15:41 GMT -->
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Aplikasi Ivantaris</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="assets/plugins/morris/morris.css">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="assets/css/style.css" rel="stylesheet" type="text/css">

    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="index.php" class="logo"><img src="assets/images/logo.png" height="24" alt="logo"></a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="assets/images/users/avatar-6.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16">Fathukalam</h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <div id="sidebar-menu">
                        <ul>
                            <li class="menu-title">Menu</li>
 <li>
                                <a href="index.php" class="waves-effect">
                                    <i class="ti-home"></i>
                                    <span> Dashboard <span class="badge badge-primary pull-right">3</span></span>
                                </a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i> <span> Pegawai</span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="data_pegawai.php">Data Pegawai</a></li>
                                  
                                </ul>
                            </li>
                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span>Petugas</span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="data_petugas.php">Data Petugas</a></li>
                               
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-crown"></i> <span> Transaksi </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="tampil_data_pinjam.php">Peminjaman</a></li>
                                    <li><a href="tampil_detail_data.php">Pengembalian</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-layout"></i><span> Ivantarisir </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                         
                                     <li><a href="tampil_data.php">Data Barang </a></li>
                                      <li><a href="data_jenis.php">Data Jenis</a></li>
                                      <li><a href="ruang.php">Data Ruang</a></li>
                                 
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span> Laporan </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="#">Laporan</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
  
                                </li>

                                <li class="list-inline-item dropdown notification-list">
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false">
                                        <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <a class="dropdown-item" href="logout.php"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                    </div>
                                </li>

                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Dashboard</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-md-6 col-xl-3">
                                    <div class="mini-stat clearfix bg-white">
                                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-cube-outline text-warning"></i></span>
                                        <div class="mini-stat-info text-right text-muted">
                                            <span class="counter text-warning">
                                               <?php
                                               $query= mysqli_query($conn,"SELECT COUNT(jumlah) FROM invantaris");
                                               $ambilquery  = mysqli_fetch_array ($query);
                                               echo $ambilquery[0];
                                               ?>
                                            </span>
                                            Data Barang
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-3">
                                    <div class="mini-stat clearfix bg-success">
                                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-account"></i></span>
                                        <div class="mini-stat-info text-right text-white">
                                            <span class="counter text-white"><?php
                                               $query= mysqli_query($conn,"SELECT COUNT(id_petugas) FROM petugas");
                                               $ambilquery  = mysqli_fetch_array ($query);
                                               echo $ambilquery[0];
                                               ?></span>
                                            Data Petugas
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-3">
                                    <div class="mini-stat clearfix bg-white">
                                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-cart-outline text-danger"></i></span>
                                        <div class="mini-stat-info text-right text-muted">
                                            <span class="counter text-danger">
                                                <?php
                                               $query= mysqli_query($conn,"SELECT COUNT(id_peminjaman) FROM peminjaman");
                                               $ambilquery  = mysqli_fetch_array ($query);
                                               echo $ambilquery[0];
                                               ?>
                                            </span>
                                            Data Peminjam
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-3">
                                    <div class="mini-stat clearfix bg-info">
                                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-account-multiple"></i></span>
                                        <div class="mini-stat-info text-right text-light">
                                            <span class="counter text-white"><?php
                                               $query= mysqli_query($conn,"SELECT COUNT(id_pegawai) FROM pegawai");
                                               $ambilquery  = mysqli_fetch_array ($query);
                                               echo $ambilquery[0];
                                               ?></span>
                                            Data Pegawai
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card m-b-3  0">
                                        <div class="card-body">
                                            <h1 class="mt-0 header-title">Data Barang</h1>
                                           
                            
                                        <div class="table-responsive"> 
                                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                     <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Kondisi</th>
                                                    <th>Spesifikasi</th>
                                                    <th>Jumlah</th>
                                                    <th>Tanggal</th>
                                                    <th>Kode Inventaris</th>
                                                    
                                                </tr>
                                                </thead>
                                                <tbody>

<?php
    include "koneksi.php";
    $no=1;
    $data1 = mysqli_query($conn, "select * from invantaris order by id_inventaris desc");
    while($data=mysqli_fetch_array($data1))
    {
    ?>

                                                 <tr>
                                            <td><?php echo $no++;?></td>
                                            <td><?php echo $data['nama'];?></td>
                                            <td><?php echo $data['kondisi'];?></td>
                                            <td><?php echo $data['spesifikasi'];?></td> 
                                            <td><?php echo $data['jumlah'];?></td>
                                            <td><?php echo $data['tgl_register'];?></td>
                                            <td><?php echo $data['kode_inventaris'];?></td>
                                           
                                        </tr> 
                                        <?php
    }
    ?>                    
                                                </tbody>
                                            </table>
                                            </div>
                                           

                                            <div id="morris-area-example" style="height: 300px"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card m-b-10">
                                        <div class="card-body">
                                            <h1 class="mt-0 header-title">Data Peminjaman</h1>

                                            <div class="table-responsive"> 
                                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                            <td>No</td>
                                            <td>ID Peminjaman</td>
                                            <td>Tanggal Peminjam</td>
                                            <td>Tanggal Kembali</td>
                                            <td>Nama Pegawai</td>
                                         
                                                </tr>
                                                </thead>
                                                <?php
                                    // koneksi
                                    $conn= mysqli_connect("localhost","root","");
                                    mysqli_select_db($conn,"ujikom");

                                    $lama = 1; // lama data yang tersimpan di database dan akan otomatis terhapus setelah 1 hari

                                    // proses untuk melakukan penghapusan data

                                    $query = "DELETE FROM peminjaman
                                              WHERE DATEDIFF(CURDATE(), tanggal_kembali) > $lama";
                                    $hasil = mysqli_query($conn,$query);
                                    ?>
                                                <tbody>

                                <?php
                                    include "koneksi.php";
                                    $query_mysqli = mysqli_query ($conn, "SELECT * FROM peminjaman INNER JOIN  pegawai on peminjaman.id_pegawai = pegawai.id_pegawai ORDER BY id_peminjaman DESC") or die (mysqli_error());
                                    $i = 1;
                                    while($data = mysqli_fetch_array($query_mysqli)){
                                ?>

                                                 <tr>
                                       <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['id_peminjaman']; ?></td>
                                    <td class="text-center"><?php echo date ('d F Y' , strtotime($data['tgl_pinjam'])); ?></td>
                                    <td class="text-center"><?php echo date ('d F Y' , strtotime($data['tgl_kembali'])); ?></td>
                                    
                                    <td class="text-center"><?php echo $data ['nama_pegawai']; ?></td>
                                    
                                    </td>
                                <?php   
                                    }
                                ?>
                                        </tr> 
                                        
                                                </tbody>
                                            </table>
                                            </div>

                                            <div id="morris-bar-example" style="height: 300px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                                    </div>
                                </div>

                            </div>
                            <!-- end row -->


                        </div><!-- container -->


                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <footer class="footer">
                    © 2019 Aplikasi Ivantaris <i class="mdi mdi-heart text-danger"></i> by Fathukalam
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/modernizr.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        
        

        <script src="assets/pages/dashborad.js"></script>

        <!-- App js -->
        <script src="assets/js/app.js"></script>

    </body>

<!-- Mirrored from themesdesign.in/upcube/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 13 Feb 2019 03:16:28 GMT -->
</html>